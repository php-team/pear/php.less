<?php

use PHPUnit\Framework\TestCase;

final class LessParserTest extends TestCase
{
    public function testCssIsSame(): void
    {
        $options = [
        'sourceMap' => false,
        ];
        $parser = new Less_Parser($options);
        $parser->parse( '@color: #36c; .link { color: @color; } a { color: @color; }' );
        $css = $parser->getCss();
        $expected = <<<'CSS'
        .link {
          color: #36c;
        }
        a {
          color: #36c;
        }

        CSS;
        self::assertSame($expected, $css);
    }

    public function testCssIsSameFromCompat(): void
    {
        $parser = new lessc();
        $css = $parser->parse('@color: #36c; .link { color: @color; } a { color: @color; }' );
        $expected = <<<'CSS'
        .link {
          color: #36c;
        }
        a {
          color: #36c;
        }

        CSS;
        self::assertSame($expected, $css);
    }
}
